# WLog #

It's a very simple logger, with log levels, the minimum of formatting and allows for dual mode (log to file and STDOUT) to support logging when running as a Docker container.

## Usage

Example

```golang
// Set up the logging
logFilePath := "mylog.log"
logFile, err := os.OpenFile(logFilePath, os.O_CREATE|os.O_APPEND|os.O_WRONLY, 0644)
if err != nil {
    log.Error("Failed to open log file '%s' with write permissions: %v", logFilePath, err)
    os.Exit(1)
}

log.SetOutput(logFile, true)
log.SetVerbose(true)

// Start using the logger
log.Info("The logger is set up")
```
