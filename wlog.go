package wlog

import (
	"fmt"
	"io"
	"log"
	"os"
	"time"
)

var verbose bool = false

// SetVerbose changes the setting whether log entries woth level DEBUG should be printed or not
func SetVerbose(isVerbose bool) {
	verbose = isVerbose
}

// SetOutput defines the writer for the log files
func SetOutput(writer io.Writer, dualMode bool) {
	log.SetFlags(0)

	if !dualMode {
		log.SetOutput(writer)
	} else {
		mw := io.MultiWriter(os.Stdout, writer)
		log.SetOutput(mw)
	}
}

// Debug adds a log entry with level name DEBUG
func Debug(format string, v ...interface{}) {
	if verbose {
		if len(v) > 0 {
			msg := fmt.Sprintf(format, v...)
			writeEntry("DEBUG", msg)
		} else {
			writeEntry("DEBUG", format)
		}
	}
}

// Info adds a log entry with level name INFO
func Info(format string, v ...interface{}) {
	if len(v) > 0 {
		msg := fmt.Sprintf(format, v...)
		writeEntry("INFO", msg)
	} else {
		writeEntry("INFO", format)
	}
}

// Warning adds a log entry with level name WARN
func Warning(format string, v ...interface{}) {
	if len(v) > 0 {
		msg := fmt.Sprintf(format, v...)
		writeEntry("WARN", msg)
	} else {
		writeEntry("WARN", format)
	}
}

// Error adds a log entry with level name ERROR
func Error(format string, v ...interface{}) {
	if len(v) > 0 {
		msg := fmt.Sprintf(format, v...)
		writeEntry("ERROR", msg)
	} else {
		writeEntry("ERROR", format)
	}
}

// Fatal adds a log entry with level name FATAL and aborts execution of the application
func Fatal(err error) {
	msg := fmt.Sprintf("Application threw an unrecoverable error: %v", err)
	writeEntry("FATAL", msg)
	log.Fatal("Abort execution of the application")
}

func writeEntry(level string, msg string) {
	var currentTime time.Time = time.Now()
	log.Printf("%-23s %-5s %s\n", currentTime.Format("2006-01-02 15:04:05.999"), level, msg)
}
